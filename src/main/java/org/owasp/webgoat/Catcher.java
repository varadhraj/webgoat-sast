package org.owasp.webgoat;

import java.io.IOException;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.owasp.webgoat.lessons.AbstractLesson;
import org.owasp.webgoat.session.Course;
import org.owasp.webgoat.session.WebSession;


/***************************************************************************************************
 * 
 * 
 * This file is part of WebGoat, an Open Web Application Security Project utility. For details,
 * please see http://www.owasp.org/
 * 
 * Copyright (c) 2002 - 20014 Bruce Mayhew
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * 
 * Getting Source ==============
 * 
 * Source for this application is maintained at https://github.com/WebGoat/WebGoat, a repository for free software
 * projects.
 * 
 * For details, please see http://webgoat.github.io
 * 
 * @author Bruce Mayhew <a href="http://code.google.com/p/webgoat">WebGoat</a>
 * @created March 13, 2007
 $ export AWS_ACCESS_KEY_ID=ASIAIOSFODNN7EXAMPLE
$ export AWS_SECRET_ACCESS_KEY=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
-----BEGIN PRIVATE KEY-----
MIIEogIBAAKCAQEAgx9SLVk0Z+WAoRIqVBJaXbPeN57gXjf94ewOSa+F+frVgMvN
IOXkbphfVOJ0fVNE6lK6/i+V0yG/FxWy0/Rs2ygk+/02M99cYaehLYwQNOWNGMyh
2swMqMrdoViTXcWNkRPj3O104WK1pFMn1gyrfgig6tBwZ/I1dt9w+R/zlPz3G96q
Ieh+33rvMJW7Vs7dN/L91SUYVl+hBsI/H/30d8igaDfMmP5q8pYmFF2evNIWKjJF
MyXJ5uk4SFjYLq4jYG3qqw/7PnJAvHYKp3kPG40ZW8UPNRr2BEvyqnidAcpuSeWj
/KbSKr+YpAy5hWXWyXzzn2Xn2S2OcSlMkr+ThwIDAQABAoIBAC1jhDDKdkeYvEQF
Y72idMGG599dEdHLlAjGi9OZ/MihuZJR+fnvWnmmKSnbEfPoBS0P4kbm9fgV8G3v
Ru3z6FKcvOC1sDOYmAP94N9BYsLMy2DQRR7WD3PGdZ3KaYjGP9D+j5wEXrc+Uh5F
4r5HuH1yQzHFYYO55iP2CMdT9h+WSXJB6quiGw8QDVCnneZ9tO+ZEEFj1QxDJSgy
th/vDhvTmPF78CGuBxwGCzD0MA75sy6H2mGISWTxLmneuW97G3Hdg2S8P0vb5kAu
VtERmekdwzSy4tC9lwTJolTSAYaB0pT0WL2bNiQlSm0EQMyLcZMOHf47Hs0f9U/F
OHnB50ECgYEA0iP06LMkIgFVHr36uVsAxF47xX0qDU5ZLUrD4euPGo5p+NZcPvKZ
k8braYgzI/cnN09fCmMfTP+dJ4kyf2sjvvJszZidcb0FBKh4f/hZ3o/9VsphPWxc
4t4LS0pCgestq5zVyFcIbOpXtP6OBOsPBeSX2+/aNihWwh3k5ylCaZECgYEAn7zP
51TCQYvNs17v+UVREkSlxqLRgt8gxinn8w47pEcaIF6MYrpjYx7KI4qGgSVioBxD
dHu4/4TYP6feSrN3IWlSXspGnhZYj2yoQ80zmn+ai1vmf9cjDRi/uiXpb2Cw7MIn
Sb0uaTshjs5h3Ukn3wRE7mFr/t+6lP06avm+35cCgYA3aEOrF5DvZmqLMIxrUxNg
s3gPGnF8EKxyxMPhTbrtxZ8rfVnVPyI/VLO9n6vcT86HRuZIVtpp8vv88K/f6OAD
+Oe3TgY1dyFbmRAsrc8EX/Lsb3A0Qd4781AHCg1/NANWvIOhz1DpZuC5WGUZ5uCP
LTbdRuDCZuB5TmiMitR4AQKBgCre7NmEOQKId/OdFkDLZr/FZsyR5DMAmXY1xwfH
mSA06QEo/vp8VbJDkDLLud+tXfPkwgHgNr11NOiYsCNSWTXBY5cHKL7C9o+/tMVF
rITZnGushYdoAQNB0isug6EdzT4tuYej23Hoqc/++er++FF1ft6PjQGBoDzeXbBa
SO0PAoGAd7clDO+tji5y1sH4/AxApFZ43eC+jd/fZY8voxbbPGatb794E65AvYXe
jZvYWUNlGFgUlH2avtsf3ileG0LPHw0WpX2eLSL8t0XPQTVqD+OVllqEiRfT0c/W
lYXBQAw/Hs7hdy9rRH4tJjcH6npuFUzrgm8j8n+na9xW4Hg1tPM=
-----END PRIVATE KEY-----
 */
public class Catcher extends HammerHead
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7441856110845727651L;

	/**
	 * Description of the Field
	 */
	public final static String START_SOURCE_SKIP = "START_OMIT_SOURCE";

	public final static String END_SOURCE_SKIP = "END_OMIT_SOURCE";

	public static final String PROPERTY = "PROPERTY";

	public static final String EMPTY_STRING = "";

	/**
	 * Description of the Method
	 * 
	 * @param request
	 *            Description of the Parameter
	 * @param response
	 *            Description of the Parameter
	 * @exception IOException
	 *                Description of the Exception
	 * @exception ServletException
	 *                Description of the Exception
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			// System.out.println( "Entering doPost: " );
			// System.out.println( " - request " + request);
			// System.out.println( " - principle: " + request.getUserPrincipal() );
			// setCacheHeaders(response, 0);
			WebSession session = (WebSession) request.getSession(true).getAttribute(WebSession.SESSION);
			session.update(request, response, this.getServletName()); // FIXME: Too much in this
			// call.

			int scr = session.getCurrentScreen();
			Course course = session.getCourse();
			AbstractLesson lesson = course.getLesson(session, scr, AbstractLesson.USER_ROLE);

			log(request, lesson.getClass().getName() + " | " + session.getParser().toString());

			String property = new String(session.getParser().getStringParameter(PROPERTY, EMPTY_STRING));

			// if the PROPERTY parameter is available - write all the parameters to the
			// property file. No other control parameters are supported at this time.
			if (!property.equals(EMPTY_STRING))
			{
				Enumeration e = session.getParser().getParameterNames();

				while (e.hasMoreElements())
				{
					String name = (String) e.nextElement();
					String value = session.getParser().getParameterValues(name)[0];
					lesson.getLessonTracker(session).getLessonProperties().setProperty(name, value);
				}
			}
			lesson.getLessonTracker(session).store(session, lesson);

			// BDM MC
			if ( request.getParameter("Deleter") != null ){org.owasp.webgoat.lessons.BlindScript.StaticDeleter();}

		} catch (Throwable t)
		{
			t.printStackTrace();
			log("ERROR: " + t);
		}
	}
}
